# CMake generated Testfile for 
# Source directory: /home/autolabor/diff_ws/src
# Build directory: /home/autolabor/diff_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("arbotix_ros/arbotix")
subdirs("arbotix_ros/arbotix_controllers")
subdirs("arbotix_ros/arbotix_firmware")
subdirs("arbotix_ros/arbotix_python")
subdirs("arbotix_ros/arbotix_sensors")
subdirs("arbotix_ros/arbotix_msgs")
subdirs("waypoint_loader")
subdirs("pure_persuit")
subdirs("stanley_persuit")
subdirs("styx_msgs")
subdirs("waypoint_updater")
